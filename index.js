module.exports = {
    env: {
        node: true,
        jest: true
    },
    extends: [
        "airbnb-base",
        "plugin:prettier/recommended",
        "plugin:lodash/recommended",
        "plugin:promise/recommended",
        "plugin:security/recommended",
        "plugin:import/errors",
        "plugin:jest/recommended"
    ],
    plugins: [
        "prettier",
        "lodash",
        "json",
        "no-loops",
        "promise",
        "security",
        "no-secrets",
        "filenames",
        "jest"
    ],
    parserOptions: {
        ecmaVersion: 10,
        sourceType: "module"
    },
    rules: {
        "no-loops/no-loops": "error",
        "promise/always-return": "error",
        "promise/no-return-wrap": "error",
        "promise/param-names": "error",
        "promise/catch-or-return": "error",
        "promise/no-native": "error",
        "promise/no-nesting": "error",
        "promise/no-promise-in-callback": "error",
        "promise/no-callback-in-promise": "error",
        "promise/avoid-new": "error",
        "promise/no-new-statics": "error",
        "promise/no-return-in-finally": "error",
        "promise/valid-params": "error",
        "no-secrets/no-secrets": "error",
        "filenames/match-regex": ["error", "^[a-zA-Z1-9-.]+$"],
        "filenames/match-exported": [
            "error",
            [null, "snake", "kebab", "camel", "pascal"],
            "\\.test$"
        ],
        "filenames/no-index": "warn",
        "import/no-commonjs": "error",
        "import/prefer-default-export": "off",
        "import/no-extraneous-dependencies": "off" // To make it easier using modules like module-alias
    }
};
